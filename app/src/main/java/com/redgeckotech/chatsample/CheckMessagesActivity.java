package com.redgeckotech.chatsample;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.redgeckotech.chatapi.ChatMessage;
import com.redgeckotech.chatapi.SimpleChat;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CheckMessagesActivity extends AppCompatActivity {

    @Bind(R.id.username) TextView usernameView;
    @Bind(R.id.check_messages) Button checkMessagesButton;
    @Bind(R.id.message_list) ListView messageListView;

    ArrayList<String> messageArray = new ArrayList<>();
    ArrayAdapter messageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_messages);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        messageAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, messageArray);
        messageListView.setAdapter(messageAdapter);
    }

    @OnClick(R.id.check_messages)
    public void checkMessagesClicked() {
        checkMessagesButton.setEnabled(false);

        String username = usernameView.getText().toString();
        Timber.d("user: %s", username);

        if (TextUtils.isEmpty(username)) {
            Toast.makeText(CheckMessagesActivity.this, "Username cannot be empty.", Toast.LENGTH_SHORT).show();
            return;
        }

        SimpleChat simpleChat = new SimpleChat.Builder()
                .baseUrl("https://damp-lake-90950.herokuapp.com/")
                .build();

        Call<List<ChatMessage>> call = simpleChat.getMessages(username);

        // Fetch and print a list of the chat messages
        call.enqueue(new Callback<List<ChatMessage>>() {
            @Override
            public void onResponse(Call<List<ChatMessage>> call, Response<List<ChatMessage>> response) {

                checkMessagesButton.setEnabled(true);

                List<ChatMessage> messages = response.body();

                if (messages != null) {
                    messageArray.clear();

                    Utility.incrementMessageReceivedCount(CheckMessagesActivity.this, messages.size());

                    for (ChatMessage chatMessage : messages) {
                        Timber.d(chatMessage.id + " (" + chatMessage.text + ")");
                        messageArray.add(chatMessage.getText());
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CheckMessagesActivity.this, R.string.messages_retrieved, Toast.LENGTH_SHORT).show();

                            messageAdapter.notifyDataSetChanged();
                        }
                    });

                } else {
                    Timber.d("No messages returned");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CheckMessagesActivity.this, R.string.no_messages_retrieved, Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<List<ChatMessage>> call, final Throwable t) {
                checkMessagesButton.setEnabled(true);

                Timber.e(t, "Could not retrieve messages.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CheckMessagesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}
