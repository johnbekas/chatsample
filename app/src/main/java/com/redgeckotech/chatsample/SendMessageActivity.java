package com.redgeckotech.chatsample;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.redgeckotech.chatapi.ChatMessage;
import com.redgeckotech.chatapi.SimpleChat;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class SendMessageActivity extends AppCompatActivity {

    @Bind(R.id.send_message) Button sendMessageButton;
    @Bind(R.id.recipient) TextView recipientView;
    @Bind(R.id.text) TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @OnClick(R.id.send_message)
    public void sendMessageClicked() {
        sendMessageButton.setEnabled(false);

        String recipient = recipientView.getText().toString();
        String text = textView.getText().toString();

        if (TextUtils.isEmpty(recipient)) {
            Toast.makeText(SendMessageActivity.this, "Recipient cannot be empty.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(text)) {
            Toast.makeText(SendMessageActivity.this, "Message cannot be empty.", Toast.LENGTH_SHORT).show();
            return;
        }

        SimpleChat simpleChat = new SimpleChat.Builder()
                .baseUrl("https://damp-lake-90950.herokuapp.com/")
                .build();

        // default to 60s for now
        Call<ChatMessage> call = simpleChat.sendMessage(recipient, text, 60);

        // Fetch and print a list of the chat messages
        call.enqueue(new Callback<ChatMessage>() {
            @Override
            public void onResponse(Call<ChatMessage> call, Response<ChatMessage> response) {

                sendMessageButton.setEnabled(true);

                ChatMessage chatMessage = response.body();

                if (chatMessage != null) {
                    Utility.incrementMessageSentCount(SendMessageActivity.this);

                    Timber.d("%s %s", getString(R.string.message_sent_successfully), chatMessage);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SendMessageActivity.this, R.string.message_sent_successfully, Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    Timber.e("Message was sent but not returned properly.");
                }
            }

            @Override
            public void onFailure(Call<ChatMessage> call, final Throwable t) {
                sendMessageButton.setEnabled(true);

                Timber.e(t, "Message was not sent.");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(SendMessageActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });


    }
}
