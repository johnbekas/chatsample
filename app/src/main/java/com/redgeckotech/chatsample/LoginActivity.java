package com.redgeckotech.chatsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class LoginActivity extends AppCompatActivity {

    private CallbackManager callbackManager;

    @Bind(R.id.login_button) LoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        loginButton.setReadPermissions("email", "public_profile");

            // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Timber.d("onSuccess");
                    Timber.d(loginResult.toString());

                    redirectToMainActivity();
                }

                @Override
                public void onCancel() {
                    Timber.d("onCancel");
                }

                @Override
                public void onError(FacebookException exception) {
                    Timber.e(exception, "onError");
                }
            });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {

        super.onStart();

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null && !accessToken.isExpired()) {
            Timber.d("access token: %s", accessToken);
            Timber.d("user id: %s", accessToken.getUserId());

            redirectToMainActivity();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (AccessToken.getCurrentAccessToken() != null) {
            Set<String> perms = AccessToken.getCurrentAccessToken().getPermissions();
            for (String perm : perms) {
                Timber.d("permission: %s", perm);
            }
        }
    }

    private void redirectToMainActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
