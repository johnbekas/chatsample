package com.redgeckotech.chatsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.redgeckotech.chatapi.ChatMessage;
import com.redgeckotech.chatapi.SimpleChat;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.profile_image) ImageView profileImage;
    @Bind(R.id.username) TextView username;
    @Bind(R.id.post_to_facebook) Button post;
    @Bind(R.id.messages_sent) TextView messagesSentView;
    @Bind(R.id.messages_received) TextView messagesReceivedView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onStart() {

        super.onStart();

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null && !accessToken.isExpired()) {
            Timber.d("access token: %s", accessToken);
            Timber.d("user id: %s", accessToken.getUserId());

            String url = String.format(Locale.US, "https://graph.facebook.com/%s/picture?type=square", accessToken.getUserId());
            Timber.d("url: %s", url);
            Picasso picasso = new Picasso.Builder(getApplicationContext())
                    //.memoryCache(cache)
                    .loggingEnabled(true)
                    .build();
            picasso.load(url).error(R.drawable.com_facebook_profile_picture_blank_portrait).fit().into(profileImage);

            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            // Application code

                            Timber.d(response.toString());
                            final String name = response.getJSONObject().optString("name", "name is N/A");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    username.setText(name);
                                }
                            });
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,link");
            request.setParameters(parameters);
            request.executeAsync();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        updateUI();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Timber.d("requestCode %d, resultCode %d, data %s", requestCode, resultCode, data);

        // 64206 is the requestCode used for publish_actions requests.
        if (requestCode == 64206 && resultCode == -1) {
            postToFacebook();
        }
    }

    @OnClick(R.id.send_message_layout)
    public void onSendMessageClicked() {
        Intent intent = new Intent(this, SendMessageActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.check_message_layout)
    public void onCheckMessagesClicked() {
        Intent intent = new Intent(this, CheckMessagesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.post_to_facebook)
    public void publishToFacebookClicked() {

        // if we have publish permissions, make post
        if (AccessToken.getCurrentAccessToken() != null) {
            Set<String> perms = AccessToken.getCurrentAccessToken().getPermissions();
            if (perms.contains("publish_actions")) {
                postToFacebook();
                return;
            }
        }

        // otherwise, request permissions.
        LoginManager.getInstance().logInWithPublishPermissions(
                this,
                Arrays.asList("publish_actions"));
    }

    public void postToFacebook() {
        if (AccessToken.getCurrentAccessToken() != null) {
            Set<String> perms = AccessToken.getCurrentAccessToken().getPermissions();
            for (String perm : perms) {
                Timber.d("permission: %s", perm);
            }
        }

        JSONObject post = new JSONObject();
        try {
            post.put("message", String.format(Locale.US, getString(R.string.message_summary),
                    Utility.getMessageSentCount(this),
                    Utility.getMessageReceivedCount(this)));

            GraphRequest postRequest = GraphRequest.newPostRequest(AccessToken.getCurrentAccessToken(),
                    "/me/feed", post, new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(final GraphResponse response) {
                            Timber.d("post response: %s", response.toString());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (response.getError() == null) {
                                        Toast.makeText(MainActivity.this, "Message posted successfully.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(MainActivity.this, response.getError().getErrorMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });

            postRequest.executeAsync();

        } catch (Exception e) {
            Timber.e(e, null);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            LoginManager.getInstance().logOut();

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateUI() {
        messagesSentView.setText(getString(R.string.messages_sent_qty, Utility.getMessageSentCount(this)));
        messagesReceivedView.setText(getString(R.string.messages_received_qty, Utility.getMessageReceivedCount(this)));
    }
}
