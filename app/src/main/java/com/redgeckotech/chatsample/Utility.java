package com.redgeckotech.chatsample;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Utility {

    public static int getMessageSentCount(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(context.getString(R.string.messages_sent_key), 0);
    }

    public static void incrementMessageSentCount(Context context) {
        int newCount = getMessageSentCount(context) + 1;

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt(context.getString(R.string.messages_sent_key), newCount);
        editor.apply();
    }

    public static int getMessageReceivedCount(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(context.getString(R.string.messages_received_key), 0);
    }

    public static void incrementMessageReceivedCount(Context context, int receivedCount) {
        int newCount = getMessageReceivedCount(context) + receivedCount;

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt(context.getString(R.string.messages_received_key), newCount);
        editor.apply();
    }
}
